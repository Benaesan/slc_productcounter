<?php 
/*
* 2016 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
	/*
	 * initiation variable
	 */

	// param sql
	/* Poste Benoit Principal */
	$source = "mysql:host=localhost;dbname=slc_dev1";
	/* Poste Benoit Portable */
	//$source = "mysql:host=localhost;dbname=test_presta";
	
	$user="root";
	$pass = "";
	/* ******************** */

	$product_id = $_GET['product_id'];

	$dates = array();

	/**
	 * Recuperation de tout les employees de type vendeur
	 */
	$SELECT_DATE = "SELECT 
							slc_date_start, slc_date_end
						FROM 
							ps_product
						WHERE 
							id_product = " . $product_id;
							

	// instanciation php data object
	$slc_db = new PDO($source, $user, $pass);

	// execution la requette SQL + recuperation du resultat de celle-ci
	$res = $slc_db->query($SELECT_DATE);
	
	/*
	 * Recupere les infos utiles sur les vendeurs,
	 */

	//while($ligne = $res->fetch()){
		$res =  $res->fetch();
		$dates['date_start'] = $res['slc_date_start'];
		$dates['date_end'] = $res['slc_date_end'];
		/*
		$employees[$ligne['id']] = array(
			'id' => $ligne['id'],
			'firstname'=> $ligne['firstname'],
			'lastname'=> $ligne['lastname'], */
			/*'phone'=> $ligne['phone'], */
			//'email'=> $ligne['email']);
			/*'address'=> $ligne['address1'], 
			'postcode'=> $ligne['postcode'], 
			'city'=> $ligne['city'], 
			'iso_code'=> $ligne['iso_code']);//*/
	//}
	
	echo json_encode($dates);

 ?>