/*
* 2015 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

/**********ATTENTE DOCUMENT READY**************/
	$(document).ready(function(){
/**********************************************/
	/** ***********************************************************************************************
	 * PARAMETRAGE
	 ** **********************************************************************************************/

	// definition url suivant les postes
	// Benoit principal
	 var urlModule = "http://127.0.0.1/slc_dev/modules/slc_productcounter";
	
	// Benoit Portable
	//var urlModule = "http://127.0.0.1/workspacePHP/test_prestashop/modules/slc_productcounter";
	
	/** ***********************************************************************************************
	 * Back Office
	 ** **********************************************************************************************/
	 // ajout de 2 champs date
	
	if($("#product_form").length != 0){
		$champUpc = $("#upc").parent().parent();

		$product_id = $('input[name=id_product]').val();


		
		labelDateDebut = '	' + 
			'<div class="form-group" id="groupDateStart">' +
			'<label class="control-label col-lg-3"  title="" for="date_start"> ' +
				'<span class="label-tooltip" data-toggle="tooltip" data-html="true">' +
					'Date du début de la vente' + 
				'</span>' +
			'</label>' +
			'<span id="slc_alert" class="warning control-label"></span>' + 
			'<div class="col-lg-3">';

		labelDateFin = '	' + 
			'<div class="form-group" id="groupDateFin">' +
			'<label class="control-label col-lg-3"  title="" for="date_end"> ' +
				'<span class="label-tooltip" data-toggle="tooltip" data-html="true">' +
					'Date Fin de la vente' + 
				'</span>' +
			'</label>' +
			'<span id="slc_alert" class="warning control-label"></span>' + 
			'<div class="col-lg-3">';

		champDateDebut = ''+
			'<div class="input-group">' +
			'<input id="date_start" class="datepicker input-medium hasDatepicker customDatePicker" name="date_start" type="text">' +
			'<span class="input-group-addon">' +
			'<i class="icon-calendar-empty"></i>' +
			'</span></div></div></div>';

		champDateFin = ' ' + 
			'<div class="input-group">' +
			'<input id="date_end" class="datepicker input-medium hasDatepicker" name="date_end" type="text">' +
			'<span class="input-group-addon">' + 
			'<i class="icon-calendar-empty"></i>' +
			'</span></div></div><input type="button" id="maj" value="MAJ"/></div>' +
			'';
		
		$champUpc.after(labelDateDebut + champDateDebut + labelDateFin + champDateFin);
		// si c'est un produit existant, on charge les dates
		if($product_id){
			$.ajax({
				url: urlModule + '/get_date.php',
				data: {product_id: $product_id},
			})
			.done(function(res) {
				product_date = JSON.parse(String(res));
				$("#date_start").val(product_date['date_start']);
				$("#date_end").val(product_date['date_end']);
				console.log("success");
			})
			.fail(function() {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		}

		// au click, sauvegarde les valeurs dans la bdd
		$("#maj").on('click', function(){
			
			//alert($('input[name=id_product]').val());
			$.ajax({
				url: urlModule + '/set_date.php',
				data: {date_start: $("#date_start").val(), date_end: $("#date_end").val(), product_id: $('input[name=id_product]').val()},
			})
			.done(function(etat) {
				console.log("success: " + etat);
			})
			.fail(function(error) {
				
				console.log("error: " + error);
			})
			.always(function() {
				console.log("complete");
			});
			
		});

		//FIXME ajout le dateTimePicker sur les champs dates
		//$('.customDatePicker').datepicker();
	}
	
/**********FIN ATTENTE DOCUMENT READY**************/
	});
/**********************************************/

	