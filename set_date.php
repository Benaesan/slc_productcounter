<?php 
/*
* 2016 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

/**
 * Met a jour la fiche produit avec la date de debut de la vente fourni
 */
	
	/*
	 * initiation variable
	 */

	
	/* Poste Benoit Principal */
	$source = "mysql:host=localhost;dbname=slc_dev1";
	/* Poste Benoit Portable */
	//$source = "mysql:host=localhost;dbname=test_presta";
	$user="root";
	$pass = "";
	/* ******************** */

	if(isset($_GET['date_start']))
		$date_start = $_GET['date_start'];
	else
		$date_start = false;
	
	if(isset($_GET['date_end']))
		$date_end = $_GET['date_end'];	
	else
		$date_end = false;

	$product_id = $_GET['product_id'];

	$UPDATE_PRODUCT = "UPDATE 
						ps_product
					SET ";

	if($date_start)
		$UPDATE_PRODUCT .= " slc_date_start = '" . $date_start . "' ";

	if($date_start && $date_end)
		$UPDATE_PRODUCT .= ", ";		

	if($date_end)
		$UPDATE_PRODUCT .= " slc_date_end = '" . $date_end . "' ";

	$UPDATE_PRODUCT .= "WHERE id_product = " . $product_id;

	// instanciation php data object
	$slc_db = new PDO($source, $user, $pass);

	// execution la requette SQL
	$res = $slc_db->query($UPDATE_PRODUCT);
	echo $UPDATE_PRODUCT;

	
 ?>