<?php
/*
* 2015 LocalAddict
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Benoit MOTTIN <benaesan@msn.com>
*  @copyright  LocalAddict
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

// source http://nemops.com/prestashop-products-new-tabs-fields

if (!defined('_PS_VERSION_'))
	exit;

class slc_productcounter extends Module
{

	/**
	 * module's constructor / Required / grant to module to install himself
	 */
	public function __construct(){

		$this->name = 'slc_productcounter';
		$this->tab = 'front_office_features';
		$this->version = '0.5.0';
		$this->author = 'Benoit MOTTIN';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;
	
		parent::__construct();
	
		$this->displayName = $this->l('SurLeChamp Compteur Produit');
		$this->description = $this->l('Ajoute un compteur sur les produits');
	
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	
	}

	/**
	 * Show module's configuration
	 */
	public function getContent(){
		// //////////////TEST////////////////
	    
	}

    /**************************************************
     * 				INSTALLATION 					  *
     **************************************************/

	/**
	 * Execute certains traitement à l'installation du module
	 */
	public function install(){
			if (!parent::install() 
				|| !$this->registerHook('displayHeader') 
				|| !$this->registerHook('displayBackOfficeHeader') 
				|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'product` ADD `slc_date_start` DATETIME NULL, ADD `slc_date_end` DATETIME NULL')
				) {
				return false;
			}
			return true;
	}

	/**
	 * Execute certains traitement à la désinstallation du module
	 */
	public function uninstall(){
		
    	if (!parent::uninstall()
    		|| !Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'product` DROP `slc_date_start`, DROP `slc_date_end` ')
    		){
				return false;
    		} 
		return true;
    	} 

	public function hookDisplayHeader($params)	{
  	    $this->context->controller->addJS($this->_path.'js/functionFO.js');
  	    $this->context->controller->addJS($this->_path.'js/jquery.countdown.js');
		$this->context->controller->addCSS($this->_path.'css/style.css');
		$this->context->controller->addJqueryUi('ui.datepicker');
	}   

	public function hookDisplayBackOfficeHeader()
	{
		 $this->context->controller->addJS($this->_path.'js/functionBO.js');
		 $this->context->controller->addJqueryUi('ui.datepicker');
	}
}
